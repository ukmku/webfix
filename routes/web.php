<?php

use App\Http\Controllers\UkmController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route UKM
Route::get('/', 'UkmController@index');
Route::get('/create', 'UkmController@create');
Route::get('/createform', 'UkmController@createform');
Route::get('/editform', 'UkmController@editform');
Route::post('/createpost', 'UkmController@createpost');
Route::get('/galery', 'UkmController@galery');
Route::get('/register', 'UkmController@register');
Route::get('/login', 'UkmController@login');
Route::get('/logout', 'UkmController@logout');
Route::post('/Postregister', 'UkmController@Postregister');
Route::post('/masuk', 'UkmController@masuk');
Route::get('/image', 'UkmController@image');
Route::post('/imagepost', 'UkmController@imagepost');
Route::get('/video', 'UkmController@video');
Route::post('/videopost', 'UkmController@videopost');
Route::get('/tentang', 'UkmController@tentang');
Route::get('/layout', 'UkmController@layout');
Route::get('/delete/{id}/deleteitem', 'UkmController@delete');
Route::get('/edit/{id}/editgalery', 'UkmController@editgalery');
Route::post('/postedit/{id}/updategalery', 'UkmController@postedit');
Route::get('/delete/{id}/deletevideo', 'UkmController@deletevideo');
// Route Admin
Route::get('/Dashboard', 'AdminController@index');
Route::get('/tentangs', 'AdminController@tentang');
Route::get('/delete/{id}/deleteukm', 'AdminController@delete');



