@extends('layouts.layout')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" />
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script> -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<div class="col-12">
  <div class="card">
    <div class="card-body">
      <div class="card-title">
        <h4>Kelola UKM</h4>
        <p>Tambah Data</p>
      </div>
      <form class="form-sample" action="/createpost" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">Nama UKM</label>
              <input disabled type="text" name="nama_ukm" class="form-control" value="{{$data['data']['name']}}" />
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">Bidang UKM</label>
              <input disabled type="text" name="category" class="form-control" value="{{$data['data']['category']}}" />
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">Logo </label>
              <div class="input-group col-xs-12">
                <span class="input-group-append">
                  <input class="file-upload-browse btn bg-blue text-white" name="image" type="file" required>
                </span>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">Contact Person</label>
              <input name="contact_person" type="text" class="form-control" placeholder="" required />
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label class="col-form-label">Latar Belakang / Deskripsi</label>
              <textarea class="form-control" id="exampleTextarea1" name="ltrblkg" rows="4" required></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <label class="col-form-label">Media Sosial</label>
            <p class="note-text">Masukkan link sosial media UKM di bawah ini</p>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="instagram">Instagram</label>
                  <input type="text" name="ig" class="col-md-9 form-control" required />
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="facebook">Facebook</label>
                  <input type="text" name="fb" class="col-md-9 form-control" required />
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="twitter">Twitter</label>
                  <input type="text" name="twitter" class="col-md-9 form-control" required />
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="website">Website</label>
                  <input type="text" name="web" class="col-md-9 form-control" required />
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="youtube">Youtube</label>
                  <input type="text" name="ytb" class="col-md-9 form-control" required />
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="youtube">email</label>
                  <input type="text" name="email" class="col-md-9 form-control" required />
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">List Kegiatan</label>
              <textarea class="form-control" name="listkg" id="exampleTextarea1" rows="4" required></textarea>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">List Prestasi</label>
              <textarea class="form-control" name="listp" id="exampleTextarea1" rows="4" required></textarea>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">Jadwal Pertemuan</label>
              <textarea class="form-control" name="jdwl" id="exampleTextarea1" rows="4" required></textarea>
            </div>
          </div>
        </div>
        <button type="submit" class="btn bg-blue text-white mr-2">Submit</button>
        <a href="/create" class="btn btn-light">Cancel</a>
      </form>
    </div>
  </div>
</div>
@endsection