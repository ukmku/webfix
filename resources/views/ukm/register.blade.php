@extends('layouts.layoutadmin')

@section('content')

<div class="card">
  <div class="card-body">
    <form id="Addfrom" action="/Postregister" method="POST" class="forms-sample">
      {{ csrf_field() }}
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label for="exampleInputUsername1">Username</label>
            <input type="text" name="username" class="form-control" id="username" placeholder="Username" required>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="exampleInputEmail1">Nama</label>
            <input type="text" name="nama_ukm" class="form-control" id="nama" placeholder="Nama" required>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Bidang UKM</label>
            <select class="form-control" name="category" id="bidang_ukm">
              <option value="senibud">Budaya dan Bahasa</option>
              <option value="kerohanian">Kerohanian</option>
              <option value="sosial">Sosial</option>
              <option value="seni">Seni</option>
              <option value="alam">Alam</option>
              <option value="kerohanian">Kerohanian</option>
              <option value="kewirausahaan">Kewirausahaan</option>
              <option value="it">IT</option>
              <option value="olahraga">Olahraga</option>
            </select>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="exampleInputPassword1">Password Confirmation</label>
            <input type="password" name="passwordulang" class="form-control" id="passwordulang" placeholder="Password" required>
          </div>
        </div>
      </div>
      <button type="submit" class="btn bg-blue text-white" data-toggle="modal" data-target="#ukmaddmodal">
        Create UKM
      </button>
    </form>
  </div>
</div>


{{-- <button type="submit"  class="btn btn-primary" data-toggle="modal" data-target="#ukmaddmodal">
                        Create UKM
                        </button>

<div class="modal fade" id="ukmaddmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <form id="Addfrom"  method="POST" class="forms-sample">
                      <div class="form-group">
                        <label for="exampleInputUsername1">Usernamedasd</label>
                        <input type="text" name="username" class="form-control" id="username" placeholder="Username" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Category</label>
                        <input type="text" name="category" class="form-control" id="category" placeholder="Category" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama</label>
                        <input type="text" name="nama_ukm" class="form-control" id="nama" placeholder="Nama" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Password Confirmation</label>
                        <input type="password" name="passwordulang" class="form-control" id="passwordulang" placeholder="Password" required>
                      </div>
                      <div class="form-check form-check-flat form-check-primary">
                        <label class="form-check-label">
                          <input type="checkbox" class="form-check-input"> Remember me </label>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-info" id="btnsave2">Save changes</button>
      </div>
    </div>
  </div>
</div> --}}
@endsection

{{-- <script src="assets/vendors/js/script.js"></script>
            <script type="text/javascript"> 
              $(document).ready(function () {
                  $('#btnsave2').on('click', function () {
                   let data = {
                  "username" : $('#username').val(),
                  "password" : $('#password').val(),
                  "passwordConfirmation" : $('#passwordulang').val(),
                  "name" : $('#nama').val(),
                  "category" : $('#category').val()
                };
                console.log(data)
                $.ajax({
                  type: "POST",
                  url:'http://178.128.222.175:8080/api/v1/communities',
                  data: data,
                  dataType: 'JSON',
                  success: function (response){
                    console.log(response)
                    $('#ukmaddmodal').modal('hide')
                  },
                  error: function(error){
                    console.log(error)
                    alert("data tidak disimpan");
                  }
                })
                });
              });
            </script> --}}