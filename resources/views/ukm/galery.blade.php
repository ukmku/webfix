@extends('layouts.layout')

@section('content')

<div class="col-12">
  <div class="card">
    <div class="card-body">
      <div class="card-title">
        <div class="row">
          @if (count($datavideo['data']) == 0)
          <div class="col-md-6">
            <h4>Kelola Foto & Video UKM</h4>
            <p>Tambah & Update Data</p>
          </div>
          <div class="col-md-3">
            <a href="/image" class="btn form-control bg-blue text-white float-center">Tambah Foto</a>
          </div>
          <div class="col-md-3">
            <a href="/video" class="btn form-control bg-blue text-white float-center">Tambah Video</a>
          </div>
          @else
          <div class="col-md-9">
            <h4>Kelola Foto & Video UKM</h4>
            <p>Tambah & Update Data</p>
          </div>
          <div class="col-md-3">
            <a href="/image" class="btn form-control bg-blue text-white float-center">Tambah Foto</a>
          </div>
          @endif
        </div>
      </div>
      <hr>
      <div class="form-sample">
        <table id="myTable" class="">
          <thead>
            <tr>
              <th>Foto</th>
              <th>Judul</th>
              <th>Keterangan</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($datalog['data'] as $item)
            <tr>
              <td><img src="{{$item['image']}}" width="150px" height="130px"></td>
              <td>{{$item['title']}}</td>
              <td>{{$item['description']}}</td>
              <td>
                <a type="button" href="/edit/{{$item["_id"]}}/editgalery" class="form-control bg-blue text-white text-center">Edit</a>
                <a type="button" href="/delete/{{$item["_id"]}}/deleteitem" onclick="return confirm('Apakah anda yakin ingin menghapus ?')" class="form-control bg-blue text-white text-center">Hapus</a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <hr>
      <div class="form-group">
        <h4>Video</h4>
        <div class="row">
          <div class="col-md-9">
          @if (count($datavideo['data']) == 0)
          <p>No data</p>
          @else
            @foreach ($datavideo['data'] as $item)
              <a href="">{{$item['video']}}</a>
          </div>
          <div class="col-md-3">
          <a type="button" href="/delete/{{$item["_id"]}}/deletevideo" onclick="return confirm('Apakah anda yakin ingin menghapus ?')" class="form-control bg-blue text-white text-center">Hapus</a>
          @endforeach
          @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('#myTable').DataTable();
  });
</script>
@endsection