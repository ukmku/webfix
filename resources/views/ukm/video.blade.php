@extends('layouts.layout')

@section('content')

<div class="card">
    <div class="card-body">
        <div class="card-title">
            <div class="row">
                <div class="col-md-6">
                    <h4>Create Video UKM</h4>
                    <p>Tambah Data</p>
                </div>
            </div>
        </div>
        <form action="/videopost" method="POST" class="forms-sample">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Link video</label>
                        <input type="text" name="video" class="form-control" name="video" placeholder="Masukkan link youtube" required>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary bg-blue" data-toggle="modal" data-target="#ukmaddmodal">
                Simpan
            </button>
        </form>
    </div>
</div>
@endsection