<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" />
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<div class="container-scroller">
  <div class="main-panel">
    <div class="content-wrapper">
      <!--content-->

      <div class="card">
        <div class="card-body">
          @foreach ($datas['data'] as $item)
          <form id="Addfrom" action="/postedit/{{$item['_id']}}/updategalery" method="POST" class="forms-sample" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
              <div class="col-md-6">

                <div class="form-group">
                  <label class="col-form-label">Logo </label>
                  <div class="input-group col-xs-12">
                    <span class="input-group-append">
                      <input class="file-upload-browse btn bg-blue text-white" name="image" type="file" required>
                      {{-- <input type="file" class="form-control-file" id="exampleFormControlFile1"> --}}
                    </span>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputEmail1">Judul</label>
                  <input type="text" name="judul" class="form-control" id="category" value="{{$item['title']}}" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputEmail1">Keterangan</label>
                  <input type="text" name="keterangan" class="form-control" id="nama" value="{{$item['description']}}" required>
                </div>
              </div>
            </div>
            @endforeach
            <button type="submit" class="btn btn-primary bg-blue" data-toggle="modal" data-target="#ukmaddmodal">
              Simpan
            </button>
          </form>
        </div>
      </div>