@extends('layouts.layout')

@section('content')
<div class="page-header">
  <h3 class="page-title">
    <span class="page-title-icon bg-gradient-primary text-white mr-2">
      <i class="mdi mdi-home"></i>

    </span> {{'Dashboard '.$data['data']['name']}} </h3>

  <nav aria-label="breadcrumb">
    <ul class="breadcrumb">
      <li class="breadcrumb-item active" aria-current="page">
        <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
      </li>
    </ul>
  </nav>
</div>

<!--  -->
<div class="row">
  <div class="col-md-4">
    <div class="card">
      @if($data['data']['logo'] == null)
      <img class="img-logo" src="assets/images/icon-logo-ukm.png" />
      @else
      <img class="img-logo" src="{{$data['data']['logo']}}" />
      @endif
      <div class="card-body">
        <h4 class="card-title text-center">{{$data['data']['name']}}</h4>
      </div>
    </div>
  </div>
  <div class="col-md-8">
    <div class="card">
      <div class="card-body">
        <h3 class="card-title">Deskripsi</h3>
        @if($data['data']['logo'] == null)
        Data Kosong
        @else
        <p>{{$data['data']['description']}}</p>
        @endif
        <hr>
        <h3 class="card-title">Jadwal Kegiatan</h3>
          @if($data['data']['logo'] == null)
          Data Kosong
          @else
          <p>{{$data['data']['schedule']}}</p>
          @endif
      </div>
    </div>
  </div>
</div>
<!--  -->
@endsection