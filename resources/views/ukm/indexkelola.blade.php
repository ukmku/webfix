@extends('layouts.layout')

@section('content')

<div class="card">
  <div class="card-body">
    <div class="card-title">
      <div class="row">
        <div class="col-md-9">
          <h4>Kelola UKM</h4>
          <p>Tambah & Update Data</p>
        </div>
        <div class="col-md-3">
          @if($data['data']['logo'] == null)
          <a href="/createform">
            <button id="btntambah" class="btn form-control bg-blue text-white float-center">Isi Data</button> </a>
          @else
          <a href="/editform">
          <button id="btnedit"  class="btn form-control bg-blue text-white float-center">Edit Data</button> </a>
          @endif
        </div>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-4">
        @if($data['data']['logo'] == null)
        <img class="img-logo" src="assets/images/icon-logo-ukm.png" />
        @else
        <img class="img-logo" src="{{$data['data']['logo']}}" />
        @endif
        <div class="card-body">
          <h4 class="card-title text-center">{{$data['data']['name']}}</h4>
        </div>
      </div>
      <div class="col-md-8">
        <div class="form-group">
          <h4 class="col-form-label">Deskripsi</h4>
          @if($data['data']['logo'] == null)
          <p>Data Kosong</p>
          @else
          <p>{{$data['data']['description']}}</p>
          @endif
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <h4 class="col-form-label">Contact Person</h4>
        @if($data['data']['logo'] == null)
        <p>Data Kosong</p>
        @else
        <p>{{$data['data']['contactPerson']}}</p>
        @endif
      </div>
      <div class="col-md-6">
        <h4 class="col-form-label">Jadwal Pertemuan</h4>
        @if($data['data']['logo'] == null)
        <p>Data Kosong</p>
        @else
        <p>{{$data['data']['schedule']}}</p>
        @endif
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <h4 class="col-form-label">List Kegiatan</h4>
        @if($data['data']['logo'] == null)
        <p>Data Kosong</p>
        @else
        <p>{{$data['data']['activities']}}</p>
        @endif
      </div>
      <div class="col-md-6">
        <h4 class="col-form-label">List Prestasi</h4>
        @if($data['data']['logo'] == null)
        <p>Data Kosong</p>
        @else
        <p>{{$data['data']['achievements']}}</p>
        @endif
      </div>
      <div class="col-md-12">
        <h4 class="col-form-label">Media Sosial</h4>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="instagram">Instagram</label>
              @if($data['data']['logo'] == null)
              <p>Data Kosong</p>
              @else
              <input disabled type="text" name="ig" class="col-md-9 form-control" value="{{$data['data']['instagram']}}" />
              @endif
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="facebook">Facebook</label>
              @if($data['data']['logo'] == null)
              <p>Data Kosong</p>
              @else
              <input disabled type="text" name="fb" class="col-md-9 form-control" value="{{$data['data']['facebook']}}" />
              @endif
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="twitter">Twitter</label>
              @if($data['data']['logo'] == null)
              <p>Data Kosong</p>
              @else
              <input disabled type="text" name="twitter" class="col-md-9 form-control" value="{{$data['data']['twitter']}}" />
              @endif
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="website">Website</label>
              @if($data['data']['logo'] == null)
              <p>Data Kosong</p>
              @else
              <input disabled type="text" name="web" class="col-md-9 form-control" value="{{$data['data']['website']}}" />
              @endif
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="youtube">Youtube</label>
              @if($data['data']['logo'] == null)
              <p>Data Kosong</p>
              @else
              <input disabled type="text" name="ytb" class="col-md-9 form-control" value="{{$data['data']['youtube']}}" />
              @endif
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="youtube">email</label>
              @if($data['data']['logo'] == null)
              <p>Data Kosong</p>
              @else
              <input disabled type="text" name="email" class="col-md-9 form-control" value="{{$data['data']['email']}}" />
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <script src="assets/vendors/js/script.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $(this).on("click", "#btntambah", function() {
        $(this).attr("disabled", "disabled");
      });

    });
  </script>
  @endsection