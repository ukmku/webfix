@extends('layouts.layout')

@section('content')
<div class="col-12">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-5">
                    <img class="img-fluid" src="assets/images/ukmku.png" alt="">
                </div>
                <div class="col-md-7" style="padding-top: 60px;">
                    <p>
                        Aplikasi UKMKU adalah aplikasi pertama di Universitas Dinamika yang dapat membantu memilih UKM yang sesuai dengan minat, bakat dan kepribadian yang anda miliki. Aplikasi ini dikembangkan oleh mahasiswa program studi S1 Sistem Informasi angkatan 2017.
                    </p>
                </div>
            </div>
            <hr>
            <div class="card-title">
                <center>
                    <h2>Meet Our Team</h2>
                    <h3 class="text-blue">UKMKU</h3>
                </center>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 text-center">
                <img class="rounded-circle img-foto-tentang" src="assets/images/photo_irvan.png" alt="">
                <div class="tulisan">
                    <h6>Irvan Alfaridzi Dwi P.</h6>
                    <p>Product Owner</p>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <img class="rounded-circle img-foto-tentang" src="assets/images/photo_denandra.png" alt="">
                <div class="tulisan">
                    <h6>Denandara Prasetya L. P.</h6>
                    <p>Lead Developer</p>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <img class="rounded-circle img-foto-tentang" src="assets/images/photo_bastian.png" alt="">
                <div class="tulisan">
                    <h6>Sebastianus Sembara</h6>
                    <p>Back End Developer</p>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <img class="rounded-circle img-foto-tentang" src="assets/images/photo_imam.png" alt="">
                <div class="tulisan">
                    <h6>M.Imam Mahudi R.</h6>
                    <p>Front End Developer</p>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <img class="rounded-circle img-foto-tentang" src="assets/images/photo_bella.png" alt="">
                <div class="tulisan">
                    <h6>Bella Ramadhanty</h6>
                    <p>UI/UX Designer</p>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <img class="rounded-circle img-foto-tentang" src="assets/images/photo_femmy.png" alt="">
                <div class="tulisan">
                    <h6>Femmy Liana P.</h6>
                    <p>UI/UX Designer</p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection