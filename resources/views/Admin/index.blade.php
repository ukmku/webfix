@extends('layouts.layoutadmin')

@section('content')
<div class="page-header">
  <h3 class="page-title">
    <span class="page-title-icon bg-gradient-primary text-white mr-2">
      <i class="mdi mdi-home"></i>
    </span> Dashboard </h3>
  <nav aria-label="breadcrumb">
    <ul class="breadcrumb">
      <li class="breadcrumb-item active" aria-current="page">
        <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
      </li>
    </ul>
  </nav>
</div>
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Username UKM</th>
            <th scope="col">Nama UKM</th>
            <th scope="col">Kategori</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($data['data'] as $item)
          <tr>
            <td>{{$item['username']}}</td>
            <td>{{$item['name']}}</td>
            <td>{{$item['category']}}</td>
            <td> <a type="button" href="/delete/{{$item["_id"]}}/deleteukm" onclick="return confirm('Apakah anda yakin ingin menghapus ?')" class="form-control bg-red text-white text-center">Hapus</a>
            </td>
          </tr>
        </tbody>
        @endforeach
      </table>
    </div>
  </div>
</div>
@endsection