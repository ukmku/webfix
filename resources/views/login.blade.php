<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>UKMKu</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="../../assets/vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="../../assets/vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <!-- endinject -->
  <!-- Layout styles -->
  <link rel="stylesheet" href="../../assets/css/style.css">
  <!-- End layout styles -->
  <link rel="shortcut icon" href="assets/images/icon-ukmku.png" />
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid">
      <div class="row auth" style="height: 100vh;">
        <div class="col-lg-4 bg-login">
        </div>
        <div class="col-lg-8 auth-form-light text-left p-5">
          <div class="brand-logo text-center">
            <img src="../../assets/images/ukmku.png">
          </div>
          <h4 class="text-center">Selamat Datang di UKMKu</h4>
          <h6 class="font-weight-light text-center">Silahkan login untuk masuk</h6>

          @if(\Session::has('alert'))
          <div class="alert alert-danger">
            <div>{{Session::get('alert')}}</div>
          </div>
          @endif
          @if(\Session::has('alert-success'))
          <div class="alert alert-success">
            <div>{{Session::get('alert-success')}}</div>
          </div>
          @endif

          <form class="pt-3" action="/masuk" method="POST">
            {{ csrf_field() }}
            <div class="row">
              <div class="col-lg-8" style="padding:70px 0;">
                <div class="form-group">
                  <input type="text" class="form-control form-control-lg" name="username" id="email" placeholder="Username" required>
                </div>
                <div class="form-group">
                  <input type="password" class="form-control form-control-lg" name="password" id="password" placeholder="Password" required>
                </div>
                <div class="mt-3">
                  <button type="submit" id="btnlogin" class="btn btn-block bg-blue text-white btn-lg font-weight-medium auth-form-btn">LOGIN</button>
                </div>
              </div>
              <div class="col-lg-4 icon-login"></div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="../../assets/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="../../assets/js/off-canvas.js"></script>
  <script src="../../assets/js/hoverable-collapse.js"></script>
  <script src="../../assets/js/misc.js"></script>
  <!-- endinject -->
</body>

</html>

{{-- function login --}}
{{-- <script src="assets/vendors/js/script.js"></script>
            <script type="text/javascript"> 
              $(document).ready(function () {
                  $('#btnlogin').on('click', function () {
                   let data = {
                  "email" : $('#email').val(),
                  "password" : $('#password').val()
                };
                console.log(data)
                $.ajax({
                  type: "POST",
                  url:'http://178.128.222.175:8080/api/v1/auth/signin',
                  data: data,
                  dataType: 'JSON',
                  success: function (response){
                    console.log(response)
                  },
                  error: function(error){
                    console.log(error)
                    alert("data tidak disimpan");
                  }
                })
                });
              });
            </script> --}}