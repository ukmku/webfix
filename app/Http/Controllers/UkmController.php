<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use GuzzleHttp;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class UkmController extends Controller
{
    
    public function index()
    {

        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $token = Session::get('token');
            $response = Http::withHeaders([
                'Authorization' => 'Bearer ' . $token[0]
            ])->get('https://ukmku.sembara.site/api/v1/communities/profile/me');
            $data = json_decode((string) $response->getBody(), true);
            return view('ukm.index')->with('data', $data);
        }
    }

    public function layout()
    {
        $token = Session::get('token');

        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $token[0]
        ])->get('https://ukmku.sembara.site/api/v1/communities/profile/me');
        $data = json_decode((string) $response->getBody(), true);
        return redirect('layout')->with('data', $data);
    }
    public function create()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $token = Session::get('token');
            $response = Http::withHeaders([
                'Authorization' => 'Bearer ' . $token[0]
            ])->get('https://ukmku.sembara.site/api/v1/communities/profile/me');
            $data = json_decode((string) $response->getBody(), true);
            return view('ukm.indexkelola')->with('data', $data);
        }
    }

    public function createpost(Request $request)
    {

        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,svg,gif'
        ]);

        // $path = $request->file('image')->store('');
        // $base64 = 'data:image/' . base64_encode($path);
        $token = session::get('token');
        $path = $request->file('image');
        $name = time() . $path->getClientOriginalName();
        $file = $path->move('public/images', $name);
        $type = pathinfo($file, PATHINFO_EXTENSION);
        $data = file_get_contents($file);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $token[0]
        ])->put('https://ukmku.sembara.site/api/v1/communities', [
            "logo" => $base64,
            "name" => $request->nama_ukm,
            "category" => $request->category,
            "description" => $request->ltrblkg,
            "schedule" => $request->jdwl,
            "activities" => $request->listkg,
            "achievements" => $request->listp,
            "contactPerson" => $request->contact_person,
            "email" => $request->email,
            "instagram" => $request->ig,
            "twitter" => $request->twitter,
            "youtube" => $request->ytb,
            "facebook" => $request->fb,
            "website" => $request->web,
        ]);
        $data = json_decode($response->getBody()->getContents());
        // dd($data);
        // return view('ukm.create');
        return redirect('create');
    }

    public function getdataprofile()
    {
        $token = 'token';
        $client = new GuzzleHttp\Client();
        $response = $client->request('GET', 'http://ukmku.sembara.site/api/v1/communities/profile/me', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $token,
            ],
            'json' => []
        ]);
    }

    public function galery()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $token = Session::get('token');
            $client = new Client([
                'headers' => ['Authorization' => 'Bearer ' . $token[0]],
            ]);
            $response = $client->request('GET', 'https://ukmku.sembara.site/api/v1/communities/profile/me');
            $data = json_decode((string) $response->getBody(), true);
            // dd($data);
            $responlog = $client->request('GET', 'https://ukmku.sembara.site/api/v1/communities/media/me/image');
            $datalog = json_decode((string) $responlog->getBody(), true);
            $responvideo = $client->request('GET', 'https://ukmku.sembara.site/api/v1/communities/media/me/video');
            $datavideo = json_decode((string) $responvideo->getBody(), true);
            // dd($datavideo);
            return view(
                'ukm.galery',
                [
                    'data' => $data,
                    'datalog' => $datalog,
                    'datavideo' => $datavideo,
                ]
            );
        }
    }
    public function galerys()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $token = Session::get('token');
            $client = new Client([
                'headers' => ['Authorization' => 'Bearer ' . $token[0]],
            ]);
            $response = $client->request('GET', 'https://ukmku.sembara.site/api/v1/communities/media/me/video');
            $data = json_decode((string) $response->getBody(), true);
            // dd($data);
            return view('ukm.galery')->with('data', $data);
        }
    }

    public function register()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            return view('ukm.register');
        }
    }
    public function login()
    {
        return view('login');
    }
    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->flush();
        return redirect('/login');
    }
    public function Postregister(Request $request)
    {
        // $request->validate([
        //     'name' => 'bail|required|unique:posts|min:6',
        //     'password' => 'required',
        //     'password' => 'required',
        //     'password' => 'required',
        //     'password' => 'required',
        // ]);
        $token = session::get('token');
        $client = new Client([
            'headers' => ['Authorization' => 'Bearer ' . $token[0]],
        ]);
        $response = $client->request('POST', 'https://ukmku.sembara.site/api/v1/communities', [
            'json' => [
                "username" => $request->username,
                "password" => $request->password,
                "passwordConfirmation" => $request->passwordulang,
                "name" => $request->nama_ukm,
                "category" => $request->category,
            ],
        ]);
        $data = json_decode($response->getBody()->getContents());
        return redirect('/Dashboard');
    }
    public function masuk(Request $request)
    {
        $admin = $request->username;
        if ($admin == 'admin@admin.com') {
            try {
                $client = new Client([
                    'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
                ]);
                $response = $client->request('POST', 'https://ukmku.sembara.site/api/v1/auth/signin', [
                    'json' => [
                        "email" => $request->username,
                        "password" => $request->password,
                    ],

                ]);

                $data = json_decode($response->getBody()->getContents());

                if (isset($data->data->token)) {
                    Session::push('token', $data->data->token);
                    Session::push('login', TRUE);
                    return redirect('/Dashboard');
                } else {
                    return redirect('login', ['data' => $data])->with('alert', 'Terjadi Kesalahan Hubungi Administrator!');
                }
            } catch (\Throwable $th) {
                // dd($th);
                return redirect('login')->with('alert', 'Password atau Email, Salah!');
            }
        } else {
            try {
                $client = new Client([
                    'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
                ]);
                $response = $client->request('POST', 'https://ukmku.sembara.site/api/v1/auth/community/signin', [
                    'json' => [
                        "username" => $request->username,
                        "password" => $request->password,
                    ],

                ]);

                $data = json_decode($response->getBody()->getContents());

                if (isset($data->data->token)) {
                    Session::push('token', $data->data->token);
                    Session::push('login', TRUE);
                    return redirect('/');
                } else {
                    return redirect('login', ['data' => $data])->with('alert', 'Terjadi Kesalahan Hubungi Administrator!');
                }
            } catch (\Throwable $th) {
                // dd($th);
                return redirect('login')->with('alert', 'Password atau Email, Salah!');
            }
        }
    }
    public function image(Request $request)
    {
        $token = Session::get('token');
        $client = new Client([
            'headers' => ['Authorization' => 'Bearer ' . $token[0]],
        ]);
        $response = $client->request('GET', 'https://ukmku.sembara.site/api/v1/communities/profile/me');
        $data = json_decode((string) $response->getBody(), true);
        $responsmed = $client->request('GET', 'https://ukmku.sembara.site/api/v1/communities/media');
        $datamed = json_decode($responsmed->getBody());
        return view('ukm.image', [
            'data' => $data,
            'datamed' => $datamed,
        ]);
    }
    public function imagepost(Request $request)
    {
        $token = Session::get('token');
        $path = $request->file('image');
        $name = time() . $path->getClientOriginalName();
        $file = $path->move('public/images', $name);
        $type = pathinfo($file, PATHINFO_EXTENSION);
        $data = file_get_contents($file);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        $client = new Client([
            'headers' => ['Authorization' => 'Bearer ' . $token[0]],
        ]);
        $response = $client->request('POST', 'https://ukmku.sembara.site/api/v1/communities/media', [
            'json' => [
                "title" => $request->judul,
                "description" => $request->keterangan,
                "image" => $base64
            ],
        ]);
        $datas = json_decode($response->getBody(), true);
        return redirect('galery');
    }

    public function delete($id)
    {

        $token = Session::get('token');
        $client = new Client([
            'headers' => ['Authorization' => 'Bearer ' . $token[0]],
        ]);
        $response = $client->delete('https://ukmku.sembara.site/api/v1/communities/media/' . $id);
        $data = json_decode((string) $response->getBody(), true);
        return redirect('galery');
    }

    public function video()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $token = Session::get('token');
            $client = new Client([
                'headers' => ['Authorization' => 'Bearer ' . $token[0]],
            ]);
            $response = $client->request('GET', 'https://ukmku.sembara.site/api/v1/communities/profile/me');
            $data = json_decode((string) $response->getBody(), true);
            // dd($data);
            return view('ukm.video')->with('data', $data);;
        }
        return view('ukm.video');
    }

    public function videopost(Request $request)
    {
        $token = Session::get('token');
        $client = new Client([
            'headers' => ['Authorization' => 'Bearer ' . $token[0]],
        ]);
        $response = $client->request('POST', 'https://ukmku.sembara.site/api/v1/communities/media', [
            'json' => [
                "video" => $request->video,
            ],
        ]);
        $data = json_decode($response->getBody(), true);
        return redirect('galery');
    }
    public function tentang()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $token = Session::get('token');
            $response = Http::withHeaders([
                'Authorization' => 'Bearer ' . $token[0]
            ])->get('https://ukmku.sembara.site/api/v1/communities/profile/me');
            $data = json_decode((string) $response->getBody(), true);
            return view('ukm.tentang')->with('data', $data);
        }
    }
    public function createform()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $token = Session::get('token');
            $response = Http::withHeaders([
                'Authorization' => 'Bearer ' . $token[0]
            ])->get('https://ukmku.sembara.site/api/v1/communities/profile/me');
            $data = json_decode((string) $response->getBody(), true);
            return view('ukm.create')->with('data', $data);
        }
    }
    public function editform()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $token = Session::get('token');
            $response = Http::withHeaders([
                'Authorization' => 'Bearer ' . $token[0]
            ])->get('https://ukmku.sembara.site/api/v1/communities/profile/me');
            $data = json_decode((string) $response->getBody(), true);
            return view('ukm.editkelola')->with('data', $data);
        }
    }
    public function editgalery($id){
        $token = Session::get('token');
        $client = new Client([
            'headers' => ['Authorization' => 'Bearer ' . $token[0]],
        ]);
        $response = $client->request('GET', 'https://ukmku.sembara.site/api/v1/communities/profile/me');
        $data = json_decode((string) $response->getBody(), true);
        $clients = new Client([
            'headers' => ['Authorization' => 'Bearer ' . $token[0]],
        ]);
        $responses = $clients->request('GET', 'https://ukmku.sembara.site/api/v1/communities/media/'.$id);
        $datas = json_decode((string) $responses->getBody(), true);
        return view('ukm.editgalery', [
            'data' => $data,
            'datas' => $datas,
        ]);
    }
    public function postedit(Request $request, $id){

        $token = Session::get('token');
        $path = $request->file('image');
        $name = time() . $path->getClientOriginalName();
        $file = $path->move('public/images', $name);
        $type = pathinfo($file, PATHINFO_EXTENSION);
        $data = file_get_contents($file);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $token[0]
        ])->put('https://ukmku.sembara.site/api/v1/communities/media/'. $id, [
            "title" =>$request->judul ,
            "description" => $request->keterangan,
            "image" => $base64,
        ]);
        $data = json_decode($response->getBody()->getContents());
        return redirect('galery');
    }

    public function deletevideo($id){
        $token = Session::get('token');
        $client = new Client([
            'headers' => ['Authorization' => 'Bearer ' . $token[0]],
        ]);
        $response = $client->delete('https://ukmku.sembara.site/api/v1/communities/media/' . $id);
        $data = json_decode((string) $response->getBody(), true);
        return redirect('galery');
    }
}


