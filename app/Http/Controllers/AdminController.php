<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use GuzzleHttp;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;


class AdminController extends Controller
{
    public function index()
    {   
        $token = Session::get('token');
        $client = new Client([
            'headers' => ['Authorization' => 'Bearer ' . $token[0]],
        ]);
        $response = $client->request('GET', 'https://ukmku.sembara.site/api/v1/communities');
        $data = json_decode((string) $response->getBody(), true);
        return view('Admin.index', ['data' => $data]);
    }
    public function tentang(){
        return view('Admin.tentang');
    }
    public function delete($id){
        $token = Session::get('token');
        $client = new Client([
            'headers' => ['Authorization' => 'Bearer ' . $token[0]],
        ]);
        $response = $client->delete('https://ukmku.sembara.site/api/v1/communities/' . $id);
        $data = json_decode((string) $response->getBody(), true);
        return redirect('Dashboard');
    }
}
